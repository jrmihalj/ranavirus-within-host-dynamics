# Author:
# Date: Mon Jul 30 10:52:28 2018
# Project Description:

#-------------------------------------------------------
#-------------------------------------------------------
# Packages
library(tidyverse)
library(rstan)
library(grid)
library(gridExtra)
library(loo)
library(MASS)

options(dplyr.width = Inf, dplyr.print_max = 1e9)
options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)
#-------------------------------------------------------
#-------------------------------------------------------

# Load and clean the data:

load_df = read_csv(file = "./data/Bullfrog_Long.csv")

## Filter only the Liver/Kidney samples

# Deal with mock dose
load_df$Dose[which(is.na(load_df$Dose))] = 0

load_clean_df = 
  load_df %>%
  filter(Sample == "LiverKidney") %>%
  # Get rid of "mock" exposure, for now,
  # because no info on initial viral dose 
  # filter(!is.na(Dose)) %>%
  # Create a numerical ID for dosage:
  mutate(Dose_ID = ifelse(Dose == 0, 1, 
                          ifelse(Dose == 1000, 2, 3))) %>%
  # For now, remove the zeros (assuming not infected):
  filter(MeanCopy > 0) %>%
  arrange(Dose_ID, SampleDay) 

#-------------------------------------------------------
#-------------------------------------------------------

# Put relevant information into vectors for Stan model:

Load = log(load_clean_df$MeanCopy+1)
Dose = load_clean_df$Dose_ID
Day = load_clean_df$SampleDay# + 1 # +1 because should start at zero
ts = c(1:max(load_clean_df$SampleDay))
n_days = length(ts)
t0 = 0
n_obs = length(Load)
#Constant = mean(Load)

# Fake data input for simulations:
n_fake = 200
fake_ts = seq(1, max(ts), length.out = n_fake)

stan_d = list(load = Load,
              dose = Dose,
              day = Day,
              n_days = n_days,
              n_params = 4,
              ts = ts,
              t0 = t0,
              n_obs = n_obs,
              n_fake = n_fake,
              fake_ts = fake_ts)

# Which parameters to monitor in the model:
params_monitor = c("Z0", "V0", 
                   "sigma_Z", "sigma_resid", "sigma_V", 
                   "params", "Sigma", "mean_corr", "Rho_params",
                   "fake_I_low", "fake_I_med", "fake_I_high",
                   "log_lik")

#-------------------------------------------------------
#-------------------------------------------------------

# Run the Stan model: 

# inits
inits_func = function(){
  list(mean_corr = c(0.1, abs(rnorm(3, 0, 2))))
}

# Test / debug the model:
test = stan("./code/Stan/WHD_modA1_log_corr.stan",
            data = stan_d,
            init = inits_func,
            pars = params_monitor,
            chains = 1, iter = 10)

# Fit and sample from the posterior
mod = stan(fit = test,
           data = stan_d,
           pars = params_monitor,
           #control=list(max_treedepth=13, adapt_delta = 0.85),
           init = inits_func,
           chains = 3,
           warmup = 2000,
           iter = 4000)#, thin = 3) 

#-------------------------------------------------------
#-------------------------------------------------------

# MCMC diagnostics:
traceplot(mod, pars="lp__")
traceplot(mod, pars=c("params"))
traceplot(mod, pars=c("Rho_params"))
pairs(mod, pars=c("params"))
traceplot(mod, pars=c("V0","Z0"))
traceplot(mod, pars=c("sigma_V", "sigma_Z", "sigma_resid"))

# hat(R)
summary(mod)$summary[,"Rhat"]


# Extract the posterior samples to a structured list:
posts = extract(mod)

# Parameter estimates:
apply(posts$params, 2, median)
apply(posts$params, 2, quantile, probs = c(0.025, 0.975))

apply(exp(posts$V0), 2, median)
apply(exp(posts$V0), 2, quantile, probs = c(0.025, 0.975))

median(exp(posts$Z0))
quantile(exp(posts$Z0), probs = c(0.025, 0.975))

#-------------------------------------------------------
#-------------------------------------------------------
##################
# WAIC and LOO-IC

#LOO-IC
looic_A1 = loo(posts$log_lik)
looic_A1$estimates
#            Estimate        SE
# elpd_loo -561.298081 11.384312
# p_loo       9.145663  2.137598
# looic    1122.596163 22.768623

waic_A1 = waic(posts$log_lik)
waic_A1$estimates
#              Estimate        SE
# elpd_waic -561.036278 11.293052
# p_waic       8.883859  1.940346
# waic      1122.072556 22.586104

#-------------------------------------------------------
#-------------------------------------------------------

#################
# Plot model fit:

# Model predictions across the sampling time period.
# These were generated with the "fake" data and time series.

V0_median = apply(posts$V0, 2, median)
V0_low = apply(posts$V0, 2, quantile, probs = c(0.025))
V0_high = apply(posts$V0, 2, quantile, probs = c(0.975))

## Low Dose
mod_low_median = apply(posts$fake_I_low[,,1], 2, median, na.rm = TRUE)
mod_low_median = c(V0_median[1], mod_low_median)

mod_low_low = apply(posts$fake_I_low[,,1], 2, quantile, probs=c(0.025), na.rm = TRUE)
mod_low_low = c(V0_low[1], mod_low_low)

mod_low_high = apply(posts$fake_I_low[,,1], 2, quantile, probs=c(0.975), na.rm = TRUE)
mod_low_high = c(V0_high[1], mod_low_high)

## Low Dose
mod_med_median = apply(posts$fake_I_med[,,1], 2, median, na.rm = TRUE)
mod_med_median = c(V0_median[2], mod_med_median)

mod_med_low = apply(posts$fake_I_med[,,1], 2, quantile, probs=c(0.025), na.rm = TRUE)
mod_med_low = c(V0_low[2], mod_med_low)

mod_med_high = apply(posts$fake_I_med[,,1], 2, quantile, probs=c(0.975), na.rm = TRUE)
mod_med_high = c(V0_high[2], mod_med_high)

## High dose
mod_high_median = apply(posts$fake_I_high[,,1], 2, median, na.rm = TRUE)
mod_high_median = c(V0_median[3], mod_high_median)

mod_high_low = apply(posts$fake_I_high[,,1], 2, quantile, probs=c(0.025), na.rm = TRUE)
mod_high_low = c(V0_low[3], mod_high_low)

mod_high_high = apply(posts$fake_I_high[,,1], 2, quantile, probs=c(0.975), na.rm = TRUE)
mod_high_high = c(V0_high[3], mod_high_high)

# Fake time:
mod_time = rep(c(0, stan_d$fake_ts), 3)
# Fake dose labels:
mod_dose = rep(c(1,2,3), each=length(mod_low_median))

# Combine into two data frames for plotting
df_fit = data.frame(mod_median = c(mod_low_median, mod_med_median, mod_high_median), 
                    mod_low = c(mod_low_low, mod_med_low, mod_high_low),
                    mod_high = c(mod_low_high, mod_med_high, mod_high_high),
                    mod_time,
                    Dose_ID = mod_dose)

#----------
# Peak timing:

peak_time = matrix(0, nrow = 3000, ncol = 3) # N_sample x N_Dose

for(j in 1:3){
  
  if(j==1) peak_temp = apply(posts$fake_I_low[,,1], 1, max, na.rm = TRUE)
  if(j==2) peak_temp = apply(posts$fake_I_med[,,1], 1, max, na.rm = TRUE)
  if(j==3) peak_temp = apply(posts$fake_I_high[,,1], 1, max, na.rm = TRUE)
  
  for(i in 1:3000){
    
    if(j==1) peak_time[i, j] = fake_ts[which(posts$fake_I_low[i,,1] == peak_temp[i])]
    if(j==2) peak_time[i, j] = fake_ts[which(posts$fake_I_med[i,,1] == peak_temp[i])]
    if(j==3) peak_time[i, j] = fake_ts[which(posts$fake_I_high[i,,1] == peak_temp[i])]
    
  }
  
}

peak_time = data.frame(peak_time)
colnames(peak_time) = c("Low", "Med", "High")

df_peak = 
  peak_time %>%
  gather(key = "Dose", value = "Peak_Time") %>%
  group_by(Dose) %>%
  summarize(peak_med = quantile(Peak_Time, probs = c(0.5)),
            peak_low = quantile(Peak_Time, probs = c(0.025)),
            peak_high = quantile(Peak_Time, probs = c(0.975)))


#----------

# Plot the synthetic data with the model predictions
# Median and 95% Credible Interval

# Plot the load data separately:
load_low = load_clean_df %>% filter(Dose == 0)
load_med = load_clean_df %>% filter(Dose == 1000)
load_high = load_clean_df %>% filter(Dose == 100000)

plot_low  =
  ggplot(load_low) +
  # Peak timing
  annotate("segment", x = as.numeric(df_peak[2, 2]), xend = as.numeric(df_peak[2, 2]), 
           y = 0.01, yend = 1e7, linetype = 2, alpha = 0.5) +
  annotate("rect", xmin = as.numeric(df_peak[2, 3]), xmax = as.numeric(df_peak[2, 4]), 
           ymin = 0.01, ymax = 1e7, fill = "red", alpha = 0.1) +
  # Data points
  geom_point(data = load_low, aes(x=SampleDay, y=(MeanCopy), shape = factor(Died)),
             col="black", size = 1.5) +
  scale_shape_manual(values = c(19,1)) + 
  guides(shape = F) +
  # Error in integration:
  geom_line(data = df_fit %>% filter(Dose_ID == 1), aes(x=mod_time, y=exp(mod_median)), color = "red") +
  geom_line(data = df_fit %>% filter(Dose_ID == 1), aes(x=mod_time, y=exp(mod_high)), color = "red", linetype=3) +
  geom_line(data = df_fit %>% filter(Dose_ID == 1), aes(x=mod_time, y=exp(mod_low)), color = "red", linetype=3) +
  # Aesthetics
  labs(x = "", y = "") + 
  scale_x_continuous(limits=c(0, 50), breaks=seq(0,50, by = 10)) + #round(maxday[1],1)
  scale_y_continuous(trans = "log", limits = c(0.01, 1e7), breaks = c(1, 1e2, 1e4, 1e6))+
  theme_classic() + 
  theme(axis.text = element_text(color = "black", size = 12)) + 
  annotate("text", x = 0, y = 1e7, label = "(a) Low Dose", hjust=0)

plot_med  =
  ggplot(load_med) +
  # Peak timing
  annotate("segment", x = as.numeric(df_peak[3, 2]), xend = as.numeric(df_peak[3, 2]), 
           y = 0.01, yend = 1e7, linetype = 2, alpha = 0.5) +
  annotate("rect", xmin = as.numeric(df_peak[3, 3]), xmax = as.numeric(df_peak[3, 4]), 
           ymin = 0.01, ymax = 1e7, fill = "red", alpha = 0.1) +
  # Data points
  geom_point(data = load_med, aes(x=SampleDay, y=(MeanCopy), shape = factor(Died)),
             col="black", size = 1.5) +
  scale_shape_manual(values = c(19,1)) + 
  guides(shape = F) +
  # Error in integration:
  geom_line(data = df_fit %>% filter(Dose_ID == 2), aes(x=mod_time, y=exp(mod_median)), color = "red") +
  geom_line(data = df_fit %>% filter(Dose_ID == 2), aes(x=mod_time, y=exp(mod_high)), color = "red", linetype=3) +
  geom_line(data = df_fit %>% filter(Dose_ID == 2), aes(x=mod_time, y=exp(mod_low)), color = "red", linetype=3) +
  # Aesthetics
  labs(x = "", y = "") + 
  scale_x_continuous(limits=c(0, 50), breaks=seq(0,50, by = 10)) + #round(maxday[2],1)
  scale_y_continuous(trans = "log", limits = c(0.01, 1e7), breaks = c(1, 1e2, 1e4, 1e6))+
  theme_classic() + 
  theme(axis.text = element_text(color = "black", size = 12)) +
  annotate("text", x = 0, y = 1e7, label = "(b) Medium Dose", hjust=0) 

plot_high  =
  ggplot(load_high) +
  # Peak timing
  annotate("segment", x = as.numeric(df_peak[1, 2]), xend = as.numeric(df_peak[1, 2]), 
           y = 0.01, yend = 1e7, linetype = 2, alpha = 0.5) +
  annotate("rect", xmin = as.numeric(df_peak[1, 3]), xmax = as.numeric(df_peak[1, 4]), 
           ymin = 0.01, ymax = 1e7, fill = "red", alpha = 0.1) +
  # Data points
  geom_point(data = load_high, aes(x=SampleDay, y=(MeanCopy), shape = factor(Died)),
             col="black", size = 1.5) +
  scale_shape_manual(values = c(19,1)) + 
  guides(shape = F) +
  # Error in integration:
  geom_line(data = df_fit %>% filter(Dose_ID == 3), aes(x=mod_time, y=exp(mod_median)), color = "red") +
  geom_line(data = df_fit %>% filter(Dose_ID == 3), aes(x=mod_time, y=exp(mod_high)), color = "red", linetype=3) +
  geom_line(data = df_fit %>% filter(Dose_ID == 3), aes(x=mod_time, y=exp(mod_low)), color = "red", linetype=3) +
  # Aesthetics
  labs(x = "", y = "") + 
  scale_x_continuous(limits=c(0, 50), breaks=seq(0,50, by = 10)) + #round(maxday[3],1)
  scale_y_continuous(trans = "log", limits = c(0.01, 1e7), breaks = c(1, 1e2, 1e4, 1e6))+
  #scale_y_continuous(limits=c(0,17), breaks=c(0,5,10,15)) +
  theme_classic() + 
  theme(axis.text = element_text(color = "black", size = 12)) +
  annotate("text", x = 0, y = 1e7, label = "(c) High Dose", hjust=0) 


plot_combo = 
  arrangeGrob(plot_low, plot_med, plot_high, ncol = 3,
              bottom = textGrob("Time (days post exposure)", just = c(0.25, -1)),
              left = textGrob("Viral Copies", rot = 90, just = c(0.25, 1)))

{
  quartz(height = 4.5, width = 12)
  grid.arrange(plot_combo)
  }

#-------------------------------------------------------
#-------------------------------------------------------

#################
# Plot trace plots:

n_sample = 2000
n_chain = 3

these_params = c(1:4)
params_df = data.frame(posts$params[,these_params])
these_names =  c("phi", "beta", "psi", "gamma")
colnames(params_df) = c("phi", "beta", "psi", "gamma")
params_df$chain = factor(rep(c(1:n_chain), each = n_sample))
params_df$iter = rep(c(2001:(2000+n_sample)), times = n_chain)

plot_traces = list()

for(i in 1:length(these_names)){
  
  sub_temp = params_df[c(i,5:6)]
  colnames(sub_temp) = c("param", "chain", "iter")
  
  plot_traces[[i]] = 
    ggplot(sub_temp) +
    geom_line(aes(x = iter, y = param, group = chain, color = chain), size = 0.1) +
    scale_color_brewer(palette = "Dark2") +
    guides(color = FALSE) +
    labs(x = "Sample", y = these_names[i])
  
}

quartz(height = 5, width = 15)
grid.arrange(arrangeGrob(grobs = plot_traces, ncol = 3))


#-------------------------------------------------------
#-------------------------------------------------------

#################
# Plot peak time (alternate)

plot_peak = 
  ggplot(df_peak) +
  geom_point(aes(x = Dose, y = peak_med)) + 
  geom_errorbar(aes(x = Dose, ymin = peak_low, ymax = peak_high), width = 0) +
  scale_x_discrete(limits = c("Low", "Med", "High")) +
  labs(x = "", y = "Time at Viral Titer Peak\n(days post exposure)") +
  theme_classic() + 
  theme(axis.text = element_text(color = "black", size = 12))

quartz(height = 3, width = 2)
plot_peak

#-------------------------------------------------------
#-------------------------------------------------------

#################
# Plot prior vs posterior for 5 main parameters

temp_mu = apply(posts$mean_corr, 2, median)
temp_Sigma = apply(posts$Sigma, c(2,3), median)

temp_sample = mvrnorm(n = n_sample*n_chain, 
                      temp_mu, temp_Sigma)

quartz(height = 6, width = 6)
par(mfrow = c(2,2))

for(i in 1:4){
  
  # if(i %in% c(1:2)) {
  #   temp = temp_sample[,i]
  #   xlim_temp = c(0, 10)
  # }else{
  #   temp = abs(rnorm(9000, 0, 1))
  #   xlim_temp = c(0, 5)
  # }
  temp = temp_sample[,i]
  xlim_temp = c(0, 10)
  
  hist(temp, breaks = 20, col = "gray", 
       xlim = xlim_temp, ylim = c(0, n_sample),
       main = these_names[i], xlab = "")
  hist(params_df[,i], breaks = 90, col = "black", add = T)
  
}

# for(i in 1:5){
#   
#   if(i %in% c(1:2)) {
#     temp = abs(rnorm(9000, 0, 5))
#     xlim_temp = c(0, 10)
#   }else{
#     temp = abs(rnorm(9000, 0, 1))
#     xlim_temp = c(0, 5)
#   }
#   
#   hist(temp, breaks = 90, col = "gray", 
#        xlim = xlim_temp, ylim = c(0, 1000),
#        main = these_names[i], xlab = "")
#   hist(params_df[,i], breaks = 90, col = "black", add = T)
#   
# }