functions {
  
  // This largely follows the deSolve package, but also includes the x_r and x_i variables.
  // These variables are used in the background.
  
  real[] WHD_1(real t,
            real[] y,
            real[] params,
            real[] x_r,
            int[] x_i) {
      
      real dydt[2];
      
      dydt[1] = params[1]  - params[2] * exp(y[2]);
      dydt[2] = ( params[3] / exp(y[2]) - params[4]) + params[5] * exp(y[1]) / exp(y[2]);
      
      return dydt;
    }
  
}

data {
  int<lower = 1> n_obs; // Number of total observations
  int<lower = 1> n_params; // Number of model parameters
  int<lower = 1> n_fake; // This is to generate "predicted"/"unsampled" data
  int<lower = 1> n_days; // Number of days sampled


  real load[n_obs]; // The viral load data
  int dose[n_obs]; // Dose ID (1, 2, 3)
  int day[n_obs]; // Day of sample
  real t0; // Initial time point (zero)
  real ts[n_days]; // Time points that were sampled
  
  real fake_ts[n_fake]; // Time points for "predicted"/"unsampled" data
  
  //real Constant;
}

transformed data {
  real x_r[0];
  int x_i[0];
}

parameters {
  //Initial conditions:
  real V0[3]; // Initial Viral Density (3 doses)
  real Z0; // Initial Immune Cell Density (not dependent on dose)
  real mean_V[3];
  real mean_Z;
  real<lower = 0> sigma_V[3]; // error for initial virus dose
  real<lower = 0> sigma_Z; // error for initial immune cell density
  // real<lower = 0, upper = 1> alpha; 
  
  //Correlated Parameters:
  vector<lower = 0>[4] sd_corr;
  cholesky_factor_corr[4] L_Rho;
  vector<lower = 0>[4] mean_corr;
  vector<lower = 0, upper = 10>[4] params_corr;
  
  real<lower = 0> sigma_resid[3]; // error in likelihood
}

transformed parameters{
  real params[n_params]; // Model parameters
  
  real y_hat_1[n_days, 2]; // Output from the ODE solver (dose 1)
  real y_hat_2[n_days, 2]; // Output from the ODE solver (dose 2)
  real y_hat_3[n_days, 2]; // Output from the ODE solver (dose 3)
  real y0_1[2]; // Initial conditions for variables
  real y0_2[2]; // Initial conditions for variables
  real y0_3[2]; // Initial conditions for variables
  real N_z;
  
  cholesky_factor_cov[4] L_Sigma; // Covariance matrix for corr params
  
  y0_1[1] = V0[1]; 
  y0_2[1] = V0[2];
  y0_3[1] = V0[3];
  
  y0_1[2] = Z0; // not dependent on dose
  y0_2[2] = Z0; // not dependent on dose
  y0_3[2] = Z0; // not dependent on dose
  
  // Correlated parameters:
  L_Sigma = diag_pre_multiply(sd_corr, L_Rho);
  
  // N_z = delta * exp(Z0)
  N_z = params_corr[3] * exp(Z0);
  
  // Assign params:
  params[1] = params_corr[1]; //phi
  params[2] = params_corr[2]; //beta
  params[3] = N_z;
  params[4] = params_corr[3]; //delta
  params[5] = params_corr[4]; //psi
  
  y_hat_1 = integrate_ode_rk45(WHD_1, y0_1, t0, ts, params, x_r, x_i);
  y_hat_2 = integrate_ode_rk45(WHD_1, y0_2, t0, ts, params, x_r, x_i);
  y_hat_3 = integrate_ode_rk45(WHD_1, y0_3, t0, ts, params, x_r, x_i);
  
}

model {
  
  // Correlated params:
  L_Rho ~ lkj_corr_cholesky(2);
  sd_corr ~ normal(0, 1);
  mean_corr ~ normal(0, 2.5);
  params_corr ~ multi_normal_cholesky(mean_corr, L_Sigma);
  
  //params constrained to be positive
  // phi ~ normal(0, 5); 
  // beta ~ normal(0, 25);  
  // delta ~ normal(0, 2);
  // psi ~ normal(0, 2);  
  // l_gamma ~ normal(0, 2);
  
  // Initial conditions:
  V0[1] ~ normal(mean_V[1], sigma_V[1]); //initial dose unknown
  V0[2] ~ normal(mean_V[2], sigma_V[2]); //initial dose log(1000)
  V0[3] ~ normal(mean_V[3], sigma_V[3]); //initial dose log(100000)
  Z0 ~ normal(mean_Z, sigma_Z); //constrained to be positive
  
  mean_V[1] ~ normal(-1, 1.5);
  mean_V[2] ~ normal(-0.5, 1.5);
  mean_V[3] ~ normal(0, 1.5);
  
  mean_Z ~ normal(0, 2);
  
  sigma_V ~ normal(0, 2);
  // sigma_V[2] ~ normal(0, 0.5);
  // sigma_V[3] ~ normal(0, 0.5);
  
  sigma_Z ~ normal(0, 5);
  sigma_resid ~ normal(0, 10);
  //alpha ~ beta(1,1);
  
  for(i in 1:n_obs){
    
    if(dose[i]==1){
      load[i] ~ normal(y_hat_1[day[i], 1], sigma_resid[1]);
    }else{
      if(dose[i]==2){
        load[i] ~ normal(y_hat_2[day[i], 1], sigma_resid[2]);
      }else{
        load[i] ~ normal(y_hat_3[day[i], 1], sigma_resid[3]);
      }
    }
  }

}

generated quantities {
  // Generate predicted data over the whole time series:
  real fake_I_low[n_fake, 2];
  real fake_I_med[n_fake, 2];
  real fake_I_high[n_fake, 2];
  // Generate likelihoods for model comparisons:
  real log_lik[n_obs];
  // Correlations between params:
  matrix[4, 4] Rho_params;
  matrix[4, 4] Sigma;
  
  fake_I_low = integrate_ode_rk45(WHD_1, y0_1, t0, fake_ts, params, x_r, x_i);
  fake_I_med = integrate_ode_rk45(WHD_1, y0_2, t0, fake_ts, params, x_r, x_i);
  fake_I_high = integrate_ode_rk45(WHD_1, y0_3, t0, fake_ts, params, x_r, x_i);
  
  for(i in 1:n_obs){
    
    if(dose[i]==1){
      log_lik[i] = normal_lpdf(load[i] | y_hat_1[day[i], 1], sigma_resid[1]);
    }else{
      if(dose[i]==2){
        log_lik[i] = normal_lpdf(load[i] | y_hat_2[day[i], 1], sigma_resid[2]);
      }else{
        log_lik[i] = normal_lpdf(load[i] | y_hat_3[day[i], 1], sigma_resid[3]);
      }
    }
  }
  
  Rho_params = multiply_lower_tri_self_transpose(L_Rho);
  Sigma = crossprod(L_Sigma);
  
}
